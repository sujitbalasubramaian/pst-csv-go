package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"reflect"
	"time"

	pst "github.com/mooijtech/go-pst/v6/pkg"
	"github.com/mooijtech/go-pst/v6/pkg/properties"
	"github.com/rotisserie/eris"
)

func main() {

	startTime := time.Now()
	fmt.Println("Initializing...")

	reader, err := os.Open("../support.pst")

	if err != nil {
		log.Fatalf("Failed to open pst file: %v\n", err)
	}

	pstFile, err := pst.New(reader)

	if err != nil {
		log.Fatalf("Failed to load pst file: %v\n", err)
	}

	defer func() {
		pstFile.Cleanup()
		if errClosing := reader.Close(); errClosing != nil {
			log.Fatalf("Failed to close PST file: %+v\n", err)
		}
	}()

	// Create attachments directory
	if _, err := os.Stat("attachments"); err != nil {
		if err := os.Mkdir("attachments", 0755); err != nil {
			log.Fatalf("Failed to create attachments directory: %+v", err)
		}
	}
	if _, err := os.Stat("out"); err != nil {
		if err := os.Mkdir("out", 0755); err != nil {
			log.Fatalf("Failed to create attachments directory: %+v", err)
		}
	}

	csvFile, err := os.Create("./out/output.csv")

	if err != nil {
		log.Fatalf("Failed to create csv file: %v", err)
	}

	defer csvFile.Close()

	writer := csv.NewWriter(csvFile)
	defer writer.Flush()

	writer.Write([]string{"From", "To", "Bcc", "Cc", "Subject", "Body", "location", "attachments"})

	// Walk through folders.
	if err := pstFile.WalkFolders(func(folder *pst.Folder) error {
		fmt.Printf("Walking folder: %s\n", folder.Name)
		messageIterator, err := folder.GetMessageIterator()

		if eris.Is(err, pst.ErrMessagesNotFound) {
			log.Printf("No messages found: %s\n", folder.Name)
			return nil
		} else if err != nil {
			return err
		}

		// Iterate through messages.
		for messageIterator.Next() {
			message := messageIterator.Value()
			fmt.Println(reflect.TypeOf(message))
			fmt.Println(reflect.TypeOf(message.Properties))
			var from, to, display_bcc, display_cc, subject, body string

			switch messageProperties := message.Properties.(type) {
			case *properties.Appointment:
				//fmt.Printf("Appointment: %s\n", messageProperties.String())
			case *properties.Contact:
				//fmt.Printf("Contact: %s\n", messageProperties.String())
			case *properties.Task:
				//fmt.Printf("Task: %s\n", messageProperties.String())
			case *properties.RSS:
				//fmt.Printf("RSS: %s\n", messageProperties.String())
			case *properties.AddressBook:
				//fmt.Printf("Address book: %s\n", messageProperties.String())
			case *properties.Message:
				from = messageProperties.GetSenderEmailAddress()
				to = messageProperties.GetReceivedByEmailAddress()
				display_bcc = messageProperties.GetDisplayBcc()
				display_cc = messageProperties.GetDisplayCc()
				subject = messageProperties.GetSubject()
				body = messageProperties.GetBody()
				writer.Write([]string{from, to, display_bcc, display_cc, subject, body})

			case *properties.Note:
				//fmt.Printf("Note: %s\n", messageProperties.String())
			default:
				fmt.Printf("Unknown message type\n")
			}
		}
		return messageIterator.Err()
	}); err != nil {
		panic(fmt.Sprintf("Failed to walk folders: %+v\n", err))
	}

	fmt.Printf("Time: %s\n", time.Since(startTime).String())

}
