module gitlab.com/sujitbalasubramanian/pst-csv-go

go 1.21.3

require (
	github.com/emersion/go-message v0.17.0
	github.com/mooijtech/go-pst/v6 v6.0.2
)

require (
	github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594 // indirect
	github.com/godzie44/go-uring v0.0.0-20220926161041-69611e8b13d5 // indirect
	github.com/libp2p/go-sockaddr v0.1.1 // indirect
	github.com/philhofer/fwd v1.1.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rotisserie/eris v0.5.4 // indirect
	github.com/tidwall/btree v1.6.0 // indirect
	github.com/tinylib/msgp v1.1.8 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.12.0 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
